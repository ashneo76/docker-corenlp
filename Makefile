.PHONY: build deploy

IMG_REPO=registry.gitlab.com
IMG_NAME=ashneo76/docker-corenlp
IMG_VERSION=3.9.1-0.0.1

build:
	docker build -t $(IMG_REPO)/$(IMG_NAME):$(IMG_VERSION) .

deploy:
	docker push $(IMG_REPO)/$(IMG_NAME):$(IMG_VERSION)
